package com.ibpd.dao;

import java.io.PrintStream;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

public class IbpdSessionFactory {
	private static String CONFIG_FILE_LOCATION = "/hibernateConfig.xml";
	private static final ThreadLocal<Session> threadLocal = new ThreadLocal();
	private static Configuration configuration = new AnnotationConfiguration();
	private static SessionFactory sessionFactory;
	private static String configFile = CONFIG_FILE_LOCATION;

	static {
		try {
			configuration.configure(configFile);
			sessionFactory = configuration.buildSessionFactory();
		} catch (Exception e) {
			System.err.println("%%%% Error Creating SessionFactory %%%%");
			e.printStackTrace();
		}
	}

	public static Session getSession() throws HibernateException {
		threadLocal.set(null);// 由于同一个session中的一级缓存无法与数据库保持一致，经常出现脏数据，这里先把一级缓存禁用掉，会对性能造成影响，完了再好好研究一下这里。
		Session session = (Session) threadLocal.get();

		if ((session == null) || (!session.isOpen())) {
			if (sessionFactory == null) {
				rebuildSessionFactory();
			}
			session = (sessionFactory != null) ? sessionFactory.openSession()
					: null;
			threadLocal.set(session);
		}

		return session;
	}

	public static void rebuildSessionFactory() {
		try {
			configuration.configure(configFile);
			sessionFactory = configuration.buildSessionFactory();
		} catch (Exception e) {
			System.err.println("%%%% Error Creating SessionFactory %%%%");
			e.printStackTrace();
		}
	}

	public static void closeSession() throws HibernateException {
		Session session = (Session) threadLocal.get();
		threadLocal.set(null);

		if (session != null)
			session.close();
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void setConfigFile(String configFile) {
		configFile = configFile;
		sessionFactory = null;
	}

	public static Configuration getConfiguration() {
		return configuration;
	}

	public static void main(String[] args) {
	}
}