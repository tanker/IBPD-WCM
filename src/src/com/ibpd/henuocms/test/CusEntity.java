package com.ibpd.henuocms.test;

public class CusEntity {

	private String 商品编码="";
	private String 合同号="";
	private String 调前价="";
	private String 调后价="";
	private String 部门编码="";
	private String 数量 ="";
	private String 起始日="";
	private String 结束日="";
	private String 供应商编码="";
	private String 备注="";
	public CusEntity(String 商品编码,String 合同号,String 调前价,String 调后价 ,String 部门编码,String 数量,String 起始日,String 结束日,String 备注,String 供应商编码){
		this.商品编码=商品编码;
		this.合同号=合同号;
		this.调前价=调前价;
		this.调后价=调后价;
		this.部门编码=部门编码;
		this.数量 =数量;
		this.起始日=起始日;
		this.结束日=结束日;
		this.备注=备注;
		this.set供应商编码(供应商编码);
	}
	public String get商品编码() {
		return 商品编码;
	}
	public void set商品编码(String 商品编码) {
		this.商品编码 = 商品编码;
	}
	public String get合同号() {
		return 合同号;
	}
	public void set合同号(String 合同号) {
		this.合同号 = 合同号;
	}
	public String get调前价() {
		return 调前价;
	}
	public void set调前价(String 调前价) {
		this.调前价 = 调前价;
	}
	public String get调后价() {
		return 调后价;
	}
	public void set调后价(String 调后价) {
		this.调后价 = 调后价;
	}
	public String get部门编码() {
		return 部门编码;
	}
	public void set部门编码(String 部门编码) {
		this.部门编码 = 部门编码;
	}
	public String get数量() {
		return 数量;
	}
	public void set数量(String 数量) {
		this.数量 = 数量;
	}
	public String get起始日() {
		return 起始日;
	}
	public void set起始日(String 起始日) {
		this.起始日 = 起始日;
	}
	public String get结束日() {
		return 结束日;
	}
	public void set结束日(String 结束日) {
		this.结束日 = 结束日;
	}
	public String get备注() {
		return 备注;
	}
	public void set备注(String 备注) {
		this.备注 = 备注;
	}
	public void set供应商编码(String 供应商编码) {
		this.供应商编码 = 供应商编码;
	}
	public String get供应商编码() {
		return 供应商编码;
	}
	public static void main(String[] args){
		 java.util.Calendar c=java.util.Calendar.getInstance();    
	     java.text.SimpleDateFormat f=new java.text.SimpleDateFormat("yyyyMMdd");    
	     System.out.println(f.format(c.getTime()));   
	}
}
