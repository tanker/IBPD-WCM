package com.ibpd.henuocms.service.nodeForm;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.NodeNodeFormEntity;
@Transactional
@Service("nodeNodeFormService")
public class NodeNodeFormServiceImpl extends BaseServiceImpl<NodeNodeFormEntity> implements INodeNodeFormService {
	public NodeNodeFormServiceImpl(){
		super();
		this.tableName="NodeNodeFormEntity";
		this.currentClass=NodeNodeFormEntity.class;
		this.initOK();
	}

} 
