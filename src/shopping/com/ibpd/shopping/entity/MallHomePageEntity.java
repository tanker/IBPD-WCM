
package com.ibpd.shopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.sf.json.JSONArray;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_mailHomePage")
public class MallHomePageEntity extends IBaseEntity{
	
	private static final long serialVersionUID = 1L;
	public static Integer TYPE_幻灯=0;
	public static Integer TYPE_今日推荐=1;
	public static Integer TYPE_热门市场=2;
	public static Integer TYPE_会员专卖=3;
	@Id @Column(name="f_id",nullable=true) @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_title",length=255,nullable=true)
	private String title="";
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_addDate",nullable=true)
	private Date addDate=new Date();
	@Column(name="f_productId",nullable=true)
	private Long productId=-1L;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getAddDate() {
		return addDate;
	}
	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	@Column(name="f_type",nullable=true)
	private Integer type=0;
	@Column(name="f_order",nullable=true)
	private Integer order=0;
	@Override
	public String toString() {
		JSONArray j=JSONArray.fromObject(this);
		return j.toString();
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
}
