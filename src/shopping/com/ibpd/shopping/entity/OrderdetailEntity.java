package com.ibpd.shopping.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import net.sf.json.JSONArray;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_orderdetail")
public class OrderdetailEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_orderID",nullable=true)
	private Long orderID;
	@Column(name="f_productID",nullable=true)
	private Long productID;
	@Column(name="f_price",nullable=true)
	private Double price;
	@Column(name="f_number",nullable=true)
	private Integer number;
	@Column(name="f_productName",length=145,nullable=true)
	private String productName;
	@Column(name="f_tenantId",nullable=true)
	private Long tenantId;
	@Column(name="f_fee",nullable=true)
	private Double fee;
	@Column(name="f_total",nullable=true)
	private Double total;
	/**
	 * 商品是否已经评价
	 */
	@Column(name="f_isComment",length=2,nullable=true)
	private String isComment="n";
	@Column(name="f_lowStocks",length=1,nullable=true)
	private String lowStocks="n";
	@Column(name="f_score",nullable=true)
	private Integer score;
	@Column(name="f_specInfo",length=100,nullable=true)
	private String specInfo;
	@Column(name="f_giftID",length=45,nullable=true)
	private String giftID;
	@Column(name="f_productImage",length=500,nullable=true)
	private String productImage;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOrderID() {
		return orderID;
	}
	public void setOrderID(Long orderID) {
		this.orderID = orderID;
	}
	public Long getProductID() {
		return productID;
	}
	public void setProductID(Long productID) {
		this.productID = productID;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Double getFee() {
		return fee;
	}
	public void setFee(Double fee) {
		this.fee = fee;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total0) {
		this.total = total0;
	}
	public String getIsComment() {
		return isComment;
	}
	public void setIsComment(String isComment) {
		this.isComment = isComment;
	}
	public String getLowStocks() {
		return lowStocks;
	}
	public void setLowStocks(String lowStocks) {
		this.lowStocks = lowStocks;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public String getSpecInfo() {
		return specInfo;
	}
	public void setSpecInfo(String specInfo) {
		this.specInfo = specInfo;
	}
	public String getGiftID() {
		return giftID;
	}
	public void setGiftID(String giftID) {
		this.giftID = giftID;
	}
	@Override
	public String toString() {
		JSONArray json=JSONArray.fromObject(this);
		return json.toString();
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public Long getTenantId() {
		return tenantId;
	}
	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}
	public String getProductImage() {
		return productImage;
	}
	
}
